__author__ = 'manfred'

##Note: All units are SI units unless otherwise specified

def isa(var, value, unit):
    ## Correct var input check:
    def varcheck(var):
        varlist=["h","p","dens"]  ##Height, Pressure, Density
        iterate=0
        for k in varlist:
            if var==k:
                iterate+=1
        return iterate
    while varcheck(var)!=1:
        varlist=["h","p","dens"]
        var=str(raw_input("Choose from height=h, pressure=p,or density=dens: "))

        
    ## Positive value check    
    while value<0:
        worddic={"h":"height","p":"pressure","d":"density"}
        value=float(raw_input("Please enter a positive %s in %s: " % (worddic[var],unit)))
    
    def unitchange(var, unit):
        ##yocto,zepto,atto,femto,pico,nano,micro,milli,centi,deci
        ##deca,hecto,kilo,mega,giga,tera,peta,exa,zetta,yotta
        prefixdic={"y":10**-24,"z":10**-21,"a":10**-18,"f":10**-15,
                 "p":10**-12,"n":10**-9,"mu":10**-6,"micro":10**-6,
                 "m":10**-3,"c":10**-2,"d":10**-1,"":10**0,"da":10**1,
                 "h":10**2,"k":10**3,"M":10**6,"G":10**9,"T":10**12,
                 "P":10**15,"E":10**18,"Z":10**21,"Y":10**24}
        vardic={"h":1,"p":2,"d":4} ##units are m,Pa,dens
        pref=unit[:-vardic[var]]
        count=0
        def prefixcheck(pref,prefixdic):
            count=0
            for check in prefixdic:
                if pref==check:
                    count+=1
            return count
        while prefixcheck(pref,prefixdic)!=1:
            pref=str(raw_input("Unrecognized prefix: Please enter a SI prefix: "))

        scaling=prefixdic[pref]
        return scaling
    
    m=unitchange(var, unit)
    x=m*value

    def getheight(var, value):
        from math import log
        g0=9.80665  ## m*s^-2
        R=287.053072  ##Gas constant of air
        T0=288.15 ##K
        p0=101325.0 ##Pa
        rho0=1.225 ##dens
        gamma=1.4
        if var=="h":
            return value
        elif var=="p":
            h=-1*log[x/p0]*(R*T0)/g0
            return h
        elif var=="dens":
            h=-1*log[x/rho0]*(R*T0)/(g0*gamma)
            return h
            
            

    alt=getheight(var, x)

    def isaheight(alt):
        ## Suggestion to make inital atmospheric layer condition list/dictionary here:
        ##     Height  Pressure  Temp  Density   Temp Gradient
        from math import exp
        
        lyrcd={0:[0.00,101325.0,288.15,1.225,-0.006468],
               1:[11000,27499.509867,216.65,0.482581443,0.0],
               2:[20100,9348.98753,216.65,0.223297899,9.917355372*(10**-4)],
               3:[32200,2227.069425,228.65,0.08014245087,0.002745098],
               4:[47300,371.7341655,270.65,0.0223102565,0.00],
               5:[52400,203.06389355,270.65,0.0144854936,-0.0019565217],
               6:[61600,68.22182319,252.65,0.0066461394,-0.0039130435],
               7:[80000,7.700245051,180.65,0.00139907744,0.00],
               8:[90000,2.352897661,180.65,0.0005998669904,0.00]}
        
        gamma=1.4
        g0=9.80665  ## m*s^-2
        R=287.053072  ##Gas constant of air
        i=0
        while i<8 and lyrcd[i+1][0]<alt:
            i+=1
        if lyrcd[8][0]<alt:
            text="This altitude is not considered in the atmosphere anymore!"
            return text

        varnames=["height","pres","temp","dens","deltatemp"] ##Variable Names
    ##    for j in range(0,len(varnames)):
    ##        str("init"+varnames[j])=lyrcd[i][j]

        initheight=lyrcd[i][0]
        initpres=lyrcd[i][1]
        inittemp=lyrcd[i][2]
        initdens=lyrcd[i][3]
        initdeltatemp=lyrcd[i][4]

            
        h=alt-initheight

        temp=inittemp+(h*initdeltatemp)
        pres=lyrcd[0][1]*(exp(-1*g0*alt/(R*lyrcd[0][2])))
        dens=lyrcd[0][3]*((pres/lyrcd[0][1])**(1/gamma))
        b=[alt,pres,temp,dens]
        return b

    varies=isaheight(alt)

    if varies=="This altitude is not considered in the atmosphere anymore!":
        return varies
    if var=="h":
        return varies
    elif var=="p":
        varies[1]=x
        return varies
    elif var=="d":
        varies[3]=x
        return varies

##def pressureunit(p):
##        print p
##        ratiobarpa = 10**(-5)  ##bar/pa
##        print "%1.5f bar" %(p*ratiobarpa)
##        ratioatmpa = 9.869*(10**-6)
##        print "%1.5f atm" %(p*ratioatmpa)


        
