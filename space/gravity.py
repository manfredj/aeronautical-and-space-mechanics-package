__author__ = 'manfred'


def earth_gravity(h=0, units='m'):
    """
    // Earth's gravity modelled roughly according to the Preliminary Reference Earth Model.
    Earth gravity by simple formula.
    """

    try:
        h = float(h)
    except ValueError:
        return "The input distance for earth_gravity is not an integer"

    r = 6371000.
    g0 = 9.8065

    if h < 0:
        return "Your distance ("+str(h)+") is negative"
    elif h > 3700000:
        return "Your distance ("+str(h)+") is outside the range this model"
    else:
        return g0*(r/(r+h))**2


def planet_gravity(mu, r, h=0, units='m'):
    """
    // Earth's gravity modelled roughly according to the Preliminary Reference Earth Model.
    Planet gravity by simple formula.
    """

    try:
        h = float(h)
        mu = float(mu)
        r = float(r)
    except ValueError:
        return "The numeric input(s) for planet_gravity is not an integer"

    g0 = mu/(r**2)

    if h < 0:
        return "Your distance ("+str(h)+") is negative"
    else:
        return g0*(r/(r+h))**2

