## This is currently only a 2-D animation of 2-body system
## Meaning that ther is no inclination on the orbits
## This is only a physical representation of Elliptical orbits

def orbitanimation(typ,info,M1,R1,M2,R2,peri,e,theta):
    ## Type (typ): "b" Two bodies with similar mass, "s" Body and satellite
                    ## where the satellite's gravitational pull is neglected
    ## Info (info): determines the inital condition with which the calculations
                    ## of the orbit are made:
                    ## "v": Velocity, "ap": Apoapsis,"a": Semi-major axis
                    ## "e": Eccentricity, "peri":Periapsis
                    ##Note: Disregard for now (usefull for expansion)
    ## Mass (M#): M1 for "stationary" body, M2 for moving body
    ## Radius (R#): R1 for "stationary" body, R2 for moving body
                    ## Note: This is only used for physical representation
    ## Periapsis (peri): Periapsis of M2 orbit
    ## Eccentricity (e): Eccentricity of M2 orbit
    ## Theta (theta): Periapsis location with respect to the velocity vector
                    ## of M1 in degrees
    import numpy as np
    import math as mt
    
    ## List of constants: (G)
    constlst=[6.67384*(10**-11)]
    def bodyorbit():
        return

    def satelliteorbit(info,M1,R2,peri,e,theta,lst):
        ## Semi-major axis
        a=peri/(1-e)
        ## Apoapsis
        apo=(2*a)-peri
        ## Gravitational Parameter
        mu=lst[0]*M1
        ## Orbital Period
        T=2*mt.pi*mt.sqrt((a**3)/mu)

        ## Visuals scaling of satellite size
        if R2==0.00:
            R2=0.05*R1
            
        a=[a,e,peri,apo,T,theta]
        return a

    if typ=="b":
        b=bodyorbit()
    elif typ=="s":
        b=satelliteorbit(info,M1,R2,peri,e,theta,constlst)

    def theorbit(orbit,R1,typ):
        import pygame
        import sys
        
        pygame.init()

        running=True
        ## The size of the window will become a function of the orbit shape
        def resolution(orbit):
            xmax=720
            ymax=720
            reso=(xmax,ymax)
            return reso

        screen = pygame.display.set_mode(resolution(orbit))
        pygame.display.set_caption("Orbit Animation")
        clock = pygame.time.Clock()
        
        ## Add background:
        background = pygame.image.load("Background1.png")
        backrect= background.get_rect()
        screen.blit(background,backrect)


        ## KSP: Credit goes to Kevin Macleod for both songs
        pygame.mixer.music.load("DeepySpace.mp3")
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(0.5)

        pygame.display.flip()
        
        while running:
       
            keypres = pygame.key.get_pressed()
            ## The next lists are used for more complex user commands
            alt_held = keypres[pygame.K_LALT] or keypres[pygame.K_RALT]
            ctrl_held = keypres[pygame.K_LCTRL] or keypres[pygame.K_RCTRL]
            
            ocount=0
            for event in pygame.event.get():

            ## This allows the user to exit the animation when they please
                if event.type == pygame.QUIT:
                    pygame.quit()


                if event.type == pygame.KEYDOWN:

                ## Secondary method for quiting the program
                    if event.key == pygame.K_ESCAPE:
                        pygame.quit()
                
                ## This will allow for the user to choose to see the orbit line
                    if event.key == pygame.K_o:
                        ocount+=1
        

                    
        
        ## Position body 1:
        
        ## Draw body 1 velocity vector:
        ## Draw orbit ellipse:

        pygame.display.flip()
    theorbit(b,R1,typ)
    
    
